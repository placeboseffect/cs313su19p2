//TODO also try with a LinkedList, does it make any difference?
LinkedList is essentially the same, but there is a slight increase in performance

//TODO what happens if you use list.remove(Integer.valueOf(77))?
This method will remove the specific value of 77 from the list.

//TODO also try with a LinkedList, does it make any difference?"
Same as above. LinkedList is essentially the same, but there is a slight increase in performance

//list.remove(5); what does this method do?
This method finds the value at index 5 inside of the list and removes it.

//list.remove(Integer.valueOf(5)); what does this one do?
This method finds the value of 5 in the list and removes it.

Which of the two lists performs better as the size increases?
Of the two the LinkedList performs better.

//Results
With ArrayList:
(Input 10):
testAverageValues = 0.002
testRemove = 0.015
testSubList = 0.005
(Input 100):
testAverageValues = 0.004
testRemove = 0.016
testSubList = 0.014
(Input 1000):
testAverageValues = 0.01
testRemove = 0.014
testSubList = 0.02
(Input 10000):
testAverageValues = 0.02
testRemove = 0.013
testSubList = 0.08
(Input 100000):
testAverageValues = 0.03
testRemove = 0.02
testSubList = 0.1


With LinkedList:
(Input 10):
testAverageValues = 0.011
testRemove = 0.001
testSubList = 0.008
(Input 100):
testAverageValues = 0.019
testRemove = 0.004
testSubList = 0.01
(Input 1000):
testAverageValues = 0.013
testRemove = 0.003
testSubList = 0.007
(Input 10000):
testAverageValues = 0.015
testRemove = 0.006
testSubList = 0.006
(Input 100000):
testAverageValues = 0.017
testRemove = 0.008
testSubList = 0.009
